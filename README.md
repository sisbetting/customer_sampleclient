# README #

*Note: using the .NET AMQP Lite API as per this example is no longer recommended, since there is now a simpler, high-level alternative: Apache NMS AMQP*

This repository contains sample code for our customers to provide a head start when integrating with our services.

**ALL SAMPLE CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF THE PARTICULAR LANGUAGE, AND FRAMEWORKS WHERE APPLICABLE, BEING PRESUMED.**

**SIS CANNOT PROVIDE GENERAL SUPPORT FOR USE OF A PARTICULAR LANGUAGE OR FRAMEWORK**


SIS Connect
===========

Please read the [High Level Overview](https://siswiki.sis.tv:8090/display/SC/High+Level+Overview) and [Client Application Design Guide](https://siswiki.sis.tv:8090/display/SC/Client+Application+Design+Guide) 
for background context. Your SIS Account Manager can provide you with login credentials. If you are unable to access these articles due to a corporate firewall, please request a copy. Also, a PDF copy of the latter (possibly not right up to date) is located in the connect folder.


Sample Code Index
=================

| Service     | Language | Framework      | Path                       | Sample Code                |
|-------------|----------|----------------|----------------------------|----------------------------|
| SIS Connect | C# .NET  | AMQP .NET Lite | connect/csharp/amqpnetlite | [Link](https://bitbucket.org/sisbetting/customer_samplecsharpeamqp.git)  |
| ""          | Java     | Spring JMS     | connect/java/springjms     | [Link](https://bitbucket.org/sisbetting/customer_samplespringjmsclient)  |
| ""          | ""       | Java EE        | connect/java/javaee        | [Link](https://bitbucket.org/sisbetting/customer_samplejavaee)           |
| ""          | ""       | Camel Java DSL | connect/java/cameljdsl     | [Link](https://bitbucket.org/sisbetting/customer_samplecameljdsl)        |


Cloning Multiple Samples
========================
Instead of navigating the above links one by one, multiple samples can be cloned at once.

For example, to fetch ALL java connect samples:

```
#!console
git clone https://bitbucket.org/sisbetting/customer_sampleclient sisegs
cd sisegs
git submodule update --init --remote connect/java/*
```

Note for SIS Developers
=======================

To include a new sample client, add a link in the table above and also add it to this repo as a submodule using the "-b master" option.