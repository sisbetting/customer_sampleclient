# README #

This part of the repository contains example clients for the SIS Connect service.

**ALL SAMPLE CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF THE PARTICULAR LANGUAGE, AND FRAMEWORKS WHERE APPLICABLE, BEING PRESUMED.**

**SIS CANNOT PROVIDE GENERAL SUPPORT FOR USE OF A PARTICULAR LANGUAGE OR FRAMEWORK**

Please read the [High Level Overview](https://siswiki.sis.tv:8090/display/SC/High+Level+Overview) and [Client Application Design Guide](https://siswiki.sis.tv:8090/display/SC/Client+Application+Design+Guide) 
for background context. Your SIS Account Manager can provide you with login credentials. If you are unable to access these articles due to a corporate firewall, a PDF copy of the latter (possibly not right up to date) is located in this folder.
